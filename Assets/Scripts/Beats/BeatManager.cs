﻿using System;
using UnityEngine;

public class BeatManager : Singleton<BeatManager> {
    public Beat activeBeat { get; set; }
    public static event Action BeatSwitchedEvent;

    void Start() {
        Init();
        ResetPublisher.ResetEvent += Init;
    }

    public void SwitchBeat(BeatIdentifier beatIdentifier) {
        Beat beat = BeatRepertoire.beats[beatIdentifier];
        Debug.Log($"BeatManager -- switching activeBeat to: {beat.name}");
        activeBeat = beat;
        BeatSwitchedEvent.Invoke();
    }

    void Init() {
        SwitchBeat(BeatIdentifier.BeginnerBeat01);
    }
}
