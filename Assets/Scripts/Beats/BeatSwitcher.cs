﻿using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;

[RequireComponent(typeof(Interactable))]
public class BeatSwitcher : MonoBehaviour {
    public BeatIdentifier beatIdentifier;

    Interactable button;
    BeatManager beatManager;

    void Start() {
        button = GetComponent<Interactable>();
        button.OnClick.AddListener(OnButtonClicked);
        beatManager = BeatManager.GetInstance();
    }

    void OnButtonClicked() {
        beatManager.SwitchBeat(beatIdentifier);
    }
}
