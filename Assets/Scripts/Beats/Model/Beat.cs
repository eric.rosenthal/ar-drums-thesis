public class Beat {
    public (Instruments instrument, Stickings sticking)[] sequence { get; set; }
    public string name { get; set; }

    public Beat((Instruments instrument, Stickings sticking)[] sequence, string name) {
        this.sequence = sequence;
        this.name = name;
    }
}
