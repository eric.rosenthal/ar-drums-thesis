public enum Instruments {
    HiHat,
    Crash,
    Ride,
    Snare,
    Kick,
    HiTom,
    MidTom,
    FloorTom
}
