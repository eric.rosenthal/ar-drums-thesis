public enum BeatIdentifier {
    BeginnerBeat01,
    BeginnerBeat02,
    IntermediateBeat01,
    IntermediateBeat02,
    AdvancedBeat01,
    AdvancedBeat02,
}
