using System.Collections.Generic;

public static class BeatRepertoire {
    public static Beat Beginner01 = new Beat(
        new (Instruments instrument, Stickings sticking)[] {
            (Instruments.FloorTom, Stickings.Right),
            (Instruments.FloorTom, Stickings.Right),
            (Instruments.Snare, Stickings.Left),
            (Instruments.Snare, Stickings.Left),
            (Instruments.FloorTom, Stickings.Right),
            (Instruments.FloorTom, Stickings.Right),
            (Instruments.Snare, Stickings.Left),
            (Instruments.Snare, Stickings.Left)
        }, "Beginner Beat #01"
    );

    public static Beat Beginner02 = new Beat(
        new (Instruments instrument, Stickings sticking)[] {
            (Instruments.Kick, Stickings.Foot),
            (Instruments.HiHat, Stickings.Right),
            (Instruments.Snare, Stickings.Left),
            (Instruments.HiHat, Stickings.Right),
            (Instruments.Kick, Stickings.Foot),
            (Instruments.HiHat, Stickings.Right),
            (Instruments.Snare, Stickings.Left),
            (Instruments.HiHat, Stickings.Right)
        }, "Beginner Beat #02"
    );

    public static Beat Intermediate01 = new Beat(
        new (Instruments instrument, Stickings sticking)[] {
            (Instruments.FloorTom, Stickings.Right),
            (Instruments.FloorTom, Stickings.Right),
            (Instruments.Snare, Stickings.Left),
            (Instruments.Snare, Stickings.Right),
            (Instruments.HiTom, Stickings.Left),
            (Instruments.HiTom, Stickings.Left),
            (Instruments.Snare, Stickings.Right),
            (Instruments.Snare, Stickings.Left)
        }, "Intermediate Beat #01"
    );

    public static Beat Intermediate02 = new Beat(
        new (Instruments instrument, Stickings sticking)[] {
            (Instruments.Kick, Stickings.Foot),
            (Instruments.HiHat, Stickings.Right),
            (Instruments.Snare, Stickings.Left),
            (Instruments.HiHat, Stickings.Right),
            (Instruments.Kick, Stickings.Foot),
            (Instruments.Kick, Stickings.Foot),
            (Instruments.Snare, Stickings.Left),
            (Instruments.HiHat, Stickings.Right)
        }, "Intermediate Beat #02"
    );

    public static Beat Advanced01 = new Beat(
        new (Instruments instrument, Stickings sticking)[] {
            (Instruments.HiTom, Stickings.Right),
            (Instruments.FloorTom, Stickings.Left),
            (Instruments.Snare, Stickings.Right),
            (Instruments.Kick, Stickings.Foot),
            (Instruments.FloorTom, Stickings.Right),
            (Instruments.MidTom, Stickings.Left),
            (Instruments.Snare, Stickings.Right),
            (Instruments.Kick, Stickings.Foot)
        }, "Advanced Beat #01"
    );

    public static Beat Advanced02 = new Beat(
        new (Instruments instrument, Stickings sticking)[] {
            (Instruments.FloorTom, Stickings.Right),
            (Instruments.Kick, Stickings.Foot),
            (Instruments.Snare, Stickings.Left),
            (Instruments.Kick, Stickings.Foot),
            (Instruments.MidTom, Stickings.Right),
            (Instruments.Kick, Stickings.Foot),
            (Instruments.Snare, Stickings.Right),
            (Instruments.HiHat, Stickings.Left)
        }, "Advanced Beat #02"
    );

    public static readonly Dictionary<BeatIdentifier, Beat> beats = new Dictionary<BeatIdentifier, Beat> {
            { BeatIdentifier.BeginnerBeat01, Beginner01 },
            { BeatIdentifier.BeginnerBeat02, Beginner02 },
            { BeatIdentifier.IntermediateBeat01, Intermediate01},
            { BeatIdentifier.IntermediateBeat02, Intermediate02},
            { BeatIdentifier.AdvancedBeat01, Advanced01 },
            { BeatIdentifier.AdvancedBeat02,  Advanced02 }
    };
}
