﻿public class ScheduledHit {
    public double scheduledTime { get; set; }
    public int count { get; set; }

    public ScheduledHit(double scheduledTime, int count) {
        this.scheduledTime = scheduledTime;
        this.count = count;
    }
}
