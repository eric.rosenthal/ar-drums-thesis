﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Metronome : MonoBehaviour {
    public const int BEATS_PER_BAR = 4;
    public const int CLICKS_PER_BAR = 8;
    public const int CHANNEL_AMOUNT = 10;
    public AudioClip oneClick;
    public AudioClip upClick;
    public AudioClip downClick;

    AudioSource[] audioSources = new AudioSource[CHANNEL_AMOUNT];
    BpmSliderController bpmSliderController;
    double bpm;
    int channel = 0;
    int count = 1;
    double nextScheduledClickTime;
    bool running = false;

    void Start() {
        bpmSliderController = BpmSliderController.GetInstance();
        bpmSliderController.BpmChangedEvent += UpdateBpmValue;
        
        PlayStateController.PlayEvent += StartMetronome;
        PlayStateController.StopEvent += StopMetronome;

        // generate new AudioPlayers on runtime as children of the Metronome
        for (int i = 0; i < CHANNEL_AMOUNT; i++) {
            GameObject child = new GameObject("AudioPlayer");
            child.transform.parent = gameObject.transform;
            audioSources[i] = child.AddComponent<AudioSource>();
        }

        bpm = bpmSliderController.GetStartValue();
    }

    void Update() {
        if (!running) return;

        double now = AudioSettings.dspTime;
        if (now + 1.0f > nextScheduledClickTime) {
            audioSources[channel].clip = count == 1 ? oneClick : (count % 2 == 0 ? downClick : upClick);
            audioSources[channel].PlayScheduled(nextScheduledClickTime);
            BeatVisualizer.ScheduleHit(new ScheduledHit(nextScheduledClickTime, count));

            nextScheduledClickTime += 60.0F / bpm / (CLICKS_PER_BAR / BEATS_PER_BAR);
            count = count == CLICKS_PER_BAR ? 1 : count + 1;
            channel = channel == CHANNEL_AMOUNT - 1 ? 0 : channel + 1;
        }
    }

    void StartMetronome() {
        Debug.Log($"Metronome -- Starting metronome");
        nextScheduledClickTime = AudioSettings.dspTime + 1.0f;
        running = true;

        // resume paused schedules at next real-time count or start at 1
        count = BeatVisualizer.realTimeCount == CLICKS_PER_BAR ? 1 : BeatVisualizer.realTimeCount + 1;
    }

    void StopMetronome() {
        Debug.Log($"Metronome -- Stopping metronome");
        running = false;
        for (int i = 0; i < CHANNEL_AMOUNT; i++) {
            audioSources[i].Stop();
        }
    }

    void ResetMetronome() {
        Debug.Log($"Metronome -- Resetting metronome");
        channel = 0;
        count = 1;
        nextScheduledClickTime = 0;
        running = false;
    }

    void UpdateBpmValue(double newValue) {
        Debug.Log($"Metronome -- set bpm to {newValue}");
        bpm = newValue;
    }
}
