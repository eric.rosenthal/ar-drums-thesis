﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BeatVisualizer : MonoBehaviour {
    [HideInInspector]
    public static int realTimeCount = 0;
    public static event Action<int> VisualizeInstrumentEvent;
    public static event Action DownBeatEvent;
    public static event Action UpBeatEvent;

    static List<ScheduledHit> scheduledHits = new List<ScheduledHit>();
    double time;
    bool running = false;
    bool currentlyIndicatingNextHit = false;

    BeatManager beatManager;
    StickingManager stickingManager;
    HitIndicatorManager hitIndicatorManager;

    void Awake() {
        time = AudioSettings.dspTime;

        PlayStateController.PlayEvent += StartVisualizer;
        PlayStateController.StopEvent += StopVisualizer;
        BeatManager.BeatSwitchedEvent += ResetVisualizer;
        ResetPublisher.ResetEvent += ResetVisualizer;
    }

    void Start() {
        beatManager = BeatManager.GetInstance();
        stickingManager = StickingManager.GetInstance();
        hitIndicatorManager = HitIndicatorManager.GetInstance();
    }

    void Update() {
        if (!running || scheduledHits.Count == 0) return;

        double now = AudioSettings.dspTime;

        // AudioSettings.dspTime has the same value for a couple of frames
        // Hence adjust the audio thread time with Time.unscaledDeltaTime as an approximation
        if (time == now || time > now) time += Time.unscaledDeltaTime;
        else time = now;

        ScheduledHit nextHit = scheduledHits[0];
        if (!currentlyIndicatingNextHit) IndicateNextHit(nextHit, now);

        // look into the future to see if the time of the next frame would already be greater than 
        // that of the scheduled hit
        if (time + Time.unscaledDeltaTime >= nextHit.scheduledTime || time == nextHit.scheduledTime) {
            VisualizeHit(nextHit);
            hitIndicatorManager.DeactivateCurrentIndicator();
            stickingManager.DeactivateCurrentSticking();
            currentlyIndicatingNextHit = false;
            realTimeCount = nextHit.count;
            scheduledHits.RemoveAt(0);
        }
    }

    public static void ScheduleHit(ScheduledHit beat) {
        scheduledHits.Add(beat);
    }

    public ScheduledHit GetFirstScheduledHit() {
        return scheduledHits[0];
    }

    public void VisualizeHit(ScheduledHit beat) {
        VisualizeInstrumentEvent.Invoke(beat.count);  
        if (beat.count % 2 == 0) UpBeatEvent.Invoke();
        else DownBeatEvent.Invoke();
    }

    void StartVisualizer() {
        Debug.Log($"BeatVisualizer -- Starting visualizer");
        running = true;
    }

    void StopVisualizer() {
        Debug.Log($"BeatVisualizer -- Stopping visualizer");
        running = false;
        scheduledHits.Clear();
    }

    void ResetVisualizer() {
        Debug.Log($"BeatVisualizer -- Resetting visualizer");
        realTimeCount = 0;
        time = 0;
        scheduledHits.Clear();
        running = false;
        currentlyIndicatingNextHit = false;
    }

    void IndicateNextHit(ScheduledHit nextHit, double now) {
        (Instruments instrument, Stickings sticking) sequenceDetails = beatManager.activeBeat.sequence[nextHit.count - 1];
        Instruments instrument = sequenceDetails.instrument;
        Stickings sticking = sequenceDetails.sticking;

        hitIndicatorManager.Indicate(instrument, nextHit.scheduledTime, now);
        stickingManager.ShowSticking(instrument, sticking);
        currentlyIndicatingNextHit = true;
    }
}
