﻿using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : Component {
    static T instance;

    public static T GetInstance() {
        if (instance == null) {
            instance = FindObjectOfType<T>();
            if (instance == null) {
                GameObject obj = new GameObject();
                obj.name = typeof(T).Name;
                instance = obj.AddComponent<T>();
            }
        }
        return instance;
    }

    protected virtual void Awake() {
        if (instance == null) {
            instance = this as T;
        } else if (instance != this as T) {
            Destroy(gameObject);
        }
    }
}
