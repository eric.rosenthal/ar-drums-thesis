﻿using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;

[RequireComponent(typeof(Interactable))]
public class PlayNavigationStateSwitcher : NavigationStateSwitcher {
    public NavigationStates toggleTo;

    protected override void Start() {
        base.Start();
        PlayStateController.TogglePlayButton += TogglePlayButton;
        PlayStateController.TogglePauseButton += TogglePauseButton;
    }

    void TogglePlayButton() {
        targetState = NavigationStates.Pause;
        toggleTo = NavigationStates.Play;

        buttonConfigHelper.MainLabelText = targetState.ToString();
        buttonConfigHelper.SetQuadIconByName(targetState.ToString());
    }

    void TogglePauseButton() {
        targetState = NavigationStates.Play;
        toggleTo = NavigationStates.Pause;

        buttonConfigHelper.MainLabelText = targetState.ToString();
        buttonConfigHelper.SetQuadIconByName(targetState.ToString());
    }
}
