﻿using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;

[RequireComponent(typeof(Interactable))]
public class NavigationStateSwitcher : MonoBehaviour {
    public NavigationStates targetState;
    protected ButtonConfigHelper buttonConfigHelper;

    Interactable button;
    NavigationStateManager navStateManager;

    protected virtual void Start() {
        button = GetComponent<Interactable>();
        button.OnClick.AddListener(OnButtonClicked);
        buttonConfigHelper = GetComponent<ButtonConfigHelper>();
        navStateManager = NavigationStateManager.GetInstance();
    }

    void OnButtonClicked() {
        Debug.Log($"NavigationStateSwitcher -- switching NavigationState to: {targetState}");
        navStateManager.SwitchState(targetState);
    }
}
