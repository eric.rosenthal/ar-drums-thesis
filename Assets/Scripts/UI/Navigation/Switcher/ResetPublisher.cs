﻿using System;
using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;

public class ResetPublisher : MonoBehaviour {
    public static event Action ResetEvent;

    Interactable button;

    void Start() {
        button = GetComponent<Interactable>();
        button.OnClick.AddListener(OnButtonClicked);
    }

    void OnButtonClicked() {
        Debug.Log("ResetPublisher - Resetting scene");
        ResetEvent.Invoke();
    }
}
