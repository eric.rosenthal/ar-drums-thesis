﻿using UnityEngine;

public class NavigationStateController : MonoBehaviour {
    public NavigationStates navigationState;

    public virtual void init() {
        deactivate();
    }

    public virtual void activate() {
        gameObject.SetActive(true);
        this.enabled = true;
    }

    public virtual void deactivate() {
        gameObject.SetActive(false);
        this.enabled = false;
    }
}
