﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.UI.BoundsControl;
using UnityEngine;

public class SetupStateInstrumentsController : NavigationStateController {
    List<BoundsControl> boundsControlList;
    List<SetupInstrumentText> instrumentTextList;
    List<ObjectManipulator> objectManipulatorComponents;

    void Awake() {
        boundsControlList = gameObject.GetComponentsInChildren<BoundsControl>().ToList();
        instrumentTextList = gameObject.GetComponentsInChildren<SetupInstrumentText>().ToList();
        objectManipulatorComponents = gameObject.GetComponentsInChildren<ObjectManipulator>().ToList();
    }

    override public void activate() {
        Debug.Log($"SetupStateController -- Activating Setup for {gameObject.name}");
        boundsControlList.ForEach(it => it.Active = true);
        instrumentTextList.ForEach(it => it.gameObject.SetActive(true));
        objectManipulatorComponents.ForEach(it => it.enabled = true);
    }

    override public void deactivate() {
        Debug.Log($"SetupStateController -- Deactivating Setup for {gameObject.name}");
        boundsControlList.ForEach(it => it.Active = false);
        instrumentTextList.ForEach(it => it.gameObject.SetActive(false));
        objectManipulatorComponents.ForEach(it => it.enabled = false);
    }
}
