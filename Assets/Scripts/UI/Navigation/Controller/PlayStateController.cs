﻿using System;
using UnityEngine;

public class PlayStateController : NavigationStateController {
    public static event Action TogglePlayButton;
    public static event Action TogglePauseButton;

    public static event Action PlayEvent;
    public static event Action StopEvent;

    override public void init() {
        Debug.Log("PlayStateController -- Deactivating PlayState");
        StopEvent?.Invoke();
    }

    override public void activate() {
        Debug.Log("PlayStateController -- Activating PlayState");
        PlayEvent?.Invoke();
        TogglePlayButton.Invoke();
    }

    override public void deactivate() {
        Debug.Log("PlayStateController -- Deactivating PlayState");
        StopEvent?.Invoke();
        TogglePauseButton.Invoke();
    }
}
