﻿using TMPro;
using UnityEngine;

public class SelectedBeatTextController : MonoBehaviour {
    string selectedBeatName;
    BeatManager beatManager;
    TMP_Text tmp;

    void Start() {
        beatManager = BeatManager.GetInstance();
        BeatManager.BeatSwitchedEvent += UpdateSelectedBeat;
        tmp = GetComponent<TMP_Text>();
    }

    void UpdateSelectedBeat() {
        selectedBeatName = beatManager.activeBeat.name;
        Debug.Log($"SelectedBeatTextController -- switching beat text to: {selectedBeatName}");
        tmp.SetText($"Selected Beat: {selectedBeatName}");
    }
}
