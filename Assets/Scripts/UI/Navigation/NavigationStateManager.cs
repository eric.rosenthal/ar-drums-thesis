﻿using System.Collections.Generic;
using System.Linq;

public enum NavigationStates {
    Play,
    Pause,
    Setup,
    Beats,
}

public class NavigationStateManager : Singleton<NavigationStateManager> {
    public NavigationStates initialState;
    List<NavigationStateController> navStateControllerList;
    List<NavigationStateController> lastActiveStateControllerList;

    protected override void Awake() {
        lastActiveStateControllerList = new List<NavigationStateController>();
        navStateControllerList = GetComponentsInChildren<NavigationStateController>().ToList();
    }

    void Start() {
        navStateControllerList.ForEach(it => it.init());
        Init();

        ResetPublisher.ResetEvent += Init;
    }

    public void SwitchState(NavigationStates state) {
        if (lastActiveStateControllerList.Any()) lastActiveStateControllerList.ForEach(it => it.deactivate());

        List<NavigationStateController> targetControllerList = navStateControllerList.FindAll(it => it.navigationState == state);
        if (targetControllerList.Any()) {
            targetControllerList.ForEach(it => it.activate());
            lastActiveStateControllerList.Clear();
            lastActiveStateControllerList = targetControllerList;
        }
    }

    void Init() {
        SwitchState(initialState);
    }
}
