using TMPro;
using UnityEngine;

public class BpmTextController : MonoBehaviour {
    BpmSliderController bpmSliderController;
    TMP_Text tmp;

    void Start() {
        bpmSliderController = BpmSliderController.GetInstance();
        bpmSliderController.BpmChangedEvent += UpdateBpm;
        tmp = GetComponent<TMP_Text>();
        tmp.SetText($"{bpmSliderController.GetStartValue()}");
    }

    void UpdateBpm(double newValue) {
        tmp.SetText($"{newValue}");
    }
}
