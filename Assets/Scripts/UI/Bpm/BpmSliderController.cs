﻿using System;
using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;

[RequireComponent(typeof(PinchSlider))]
public class BpmSliderController : Singleton<BpmSliderController> {
    public event Action<double> BpmChangedEvent;

    const int LOWER_BPM_LIMIT = 20;
    const int UPPER_BPM_LIMIT = 100;

    void Start() {
        gameObject.SetActive(false);
    }

    public void PropagateBpmChange(SliderEventData sliderEvent) {
        float sliderValue = sliderEvent.NewValue;
        double newBpmValue = Math.Round(sliderValue * (UPPER_BPM_LIMIT - LOWER_BPM_LIMIT), 0, MidpointRounding.ToEven) + LOWER_BPM_LIMIT;
        BpmChangedEvent.Invoke(newBpmValue);
    }

    public double GetStartValue() {
        return (LOWER_BPM_LIMIT + UPPER_BPM_LIMIT) / 2;
    }

    public void toggleVisuals() {
        gameObject.SetActive(!gameObject.activeSelf);
    }
}
