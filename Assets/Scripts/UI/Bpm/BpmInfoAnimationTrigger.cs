﻿using UnityEngine;

public class BpmInfoAnimationTrigger : MonoBehaviour {
    Animator animator;

    void Start() {
        animator = GetComponent<Animator>();
        BeatVisualizer.DownBeatEvent += TriggerDownBeatAnimation;
        BeatVisualizer.UpBeatEvent += TriggerUpBeatAnimation;
    }

    void TriggerDownBeatAnimation() {
        animator.SetTrigger("DownbeatPulse");
    }

    void TriggerUpBeatAnimation() {
        animator.SetTrigger("UpbeatPulse");
    }
}
