using UnityEngine;

public class HitIndicatorController : MonoBehaviour {
    public Instruments instrument;

    Animator animator;
    const int ANIMATION_LENGTH = 1;

    void Awake() {
        animator = gameObject.GetComponent<Animator>();
    }

    public virtual void Activate() {
        gameObject.SetActive(true);
    }

    public virtual void Deactivate() {
        gameObject.SetActive(false);
    }

    // animation clip lasts one second - time values are seconds as well
    public virtual void Animate(double scheduledHitTime, double now) { 
        double deltaTime = scheduledHitTime - now;
        animator.speed = (float) (ANIMATION_LENGTH / deltaTime);
        animator.SetTrigger("Play");
    }
}
