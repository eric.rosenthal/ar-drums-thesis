using UnityEngine;

public class InstrumentPositionController : MonoBehaviour {
    Vector3 startPosition;
    Quaternion startRotation;
    Vector3 startScale;

    void Awake() {
        startPosition = transform.localPosition;
        startRotation = transform.localRotation;
        startScale = transform.localScale;
    }

    public void Reset() {
        transform.localPosition = startPosition;
        transform.localRotation = startRotation;
        transform.localScale = startScale;
    }
}
