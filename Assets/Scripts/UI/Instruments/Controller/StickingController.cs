using TMPro;
using UnityEngine;

public class StickingController : MonoBehaviour {
    Transform cam;
    TMP_Text tmp;

    void Start() {
        cam = Camera.main.transform;
        tmp = GetComponent<TMP_Text>();
    }

    void Update() {
        if (!gameObject.activeSelf) return; 
        
        transform.LookAt(cam);
        // rotate again since text is flipped
        transform.Rotate(Vector3.up - new Vector3(0,180,0));
    }

    public void SetStickingText(string sticking) {
        tmp.SetText(sticking);
    }

    public void Activate() {
        gameObject.SetActive(true);
    }

    public void Deactivate() {
        gameObject.SetActive(false);
    }
}
