using System.Collections.Generic;

public class StickingManager : Singleton<StickingManager> {
    StickingController currentActiveStickingController;
    Dictionary<Instruments, StickingController> stickingControllers;
    Dictionary<Stickings, string> stickingTextMapping;

    void Start() {
        stickingControllers = new Dictionary<Instruments, StickingController> {
            { Instruments.HiHat, gameObject.transform.Find(Instruments.HiHat.ToString()).GetComponentInChildren<StickingController>() },
            { Instruments.Snare, gameObject.transform.Find(Instruments.Snare.ToString()).GetComponentInChildren<StickingController>() },
            { Instruments.HiTom, gameObject.transform.Find(Instruments.HiTom.ToString()).GetComponentInChildren<StickingController>() },
            { Instruments.MidTom, gameObject.transform.Find(Instruments.MidTom.ToString()).GetComponentInChildren<StickingController>() },
            { Instruments.FloorTom, gameObject.transform.Find(Instruments.FloorTom.ToString()).GetComponentInChildren<StickingController>() },
            { Instruments.Kick, gameObject.transform.Find(Instruments.Kick.ToString()).GetComponentInChildren<StickingController>() }
        };

        stickingTextMapping = new Dictionary<Stickings, string> {
            { Stickings.Right, "R" },
            { Stickings.Left, "L" },
            { Stickings.Foot, "F" }
        };

        Reset();

        BeatManager.BeatSwitchedEvent += DeactivateCurrentSticking;
        ResetPublisher.ResetEvent += Reset;
    }

    public void ShowSticking(Instruments instrument, Stickings sticking) {
        StickingController controller = stickingControllers[instrument];
        controller.SetStickingText(stickingTextMapping[sticking]);
        controller.Activate();
        
        currentActiveStickingController = controller;
    }

    public void DeactivateCurrentSticking() {
        currentActiveStickingController?.Deactivate();
    }

    void Reset() {
        currentActiveStickingController?.Deactivate();
        currentActiveStickingController = null;
    }
}
