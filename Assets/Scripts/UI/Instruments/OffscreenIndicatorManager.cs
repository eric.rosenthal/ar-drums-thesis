using Microsoft.MixedReality.Toolkit.Experimental.Utilities;
using UnityEngine;

public class OffscreenIndicatorManager : Singleton<OffscreenIndicatorManager> {
    Transform currentTarget;
    DirectionalIndicator directionalIndicator;

    Vector3 MAX_SCALE = new Vector3(0.045f, 0.045f, 0.045f);
    Vector3 MIN_SCALE = new Vector3(0.005f, 0.005f, 0.005f);
    Vector3 scaleMargin;

    const float MAX_DISTANCE = 0.75f;
    const float MIN_DISTANCE = 0.25f;
    float distanceMargin;
    

    void Start() {
        directionalIndicator = gameObject.GetComponent<DirectionalIndicator>();
        scaleMargin = MAX_SCALE - MIN_SCALE;
        distanceMargin = MAX_DISTANCE - MIN_DISTANCE;

        PlayStateController.PlayEvent += Activate;
        PlayStateController.StopEvent += Deactivate;
        Deactivate();
    }

    // scale the indicator based on its distance to the current target
    void Update() {
        if (!gameObject.activeSelf || (currentTarget == null)) return;

        float  distanceBetweenIndicatorAndTarget = Vector3.Distance(transform.position, currentTarget.position);

        if (distanceBetweenIndicatorAndTarget >= MAX_DISTANCE) transform.localScale = MAX_SCALE;
        else if (distanceBetweenIndicatorAndTarget <= MIN_DISTANCE) transform.localScale = MIN_SCALE;
        else {
            float distanceFromMin = distanceBetweenIndicatorAndTarget - MIN_DISTANCE;
            float scalePercentage = distanceFromMin / distanceMargin;
            Vector3 scaleAdjustment = scaleMargin * scalePercentage;

            transform.localScale = MIN_SCALE + scaleAdjustment;
        }
    }

    public virtual void Activate() {
        gameObject.SetActive(true);
    }

    public virtual void Deactivate() {
        gameObject.SetActive(false);
    }

    public virtual void ChangeTarget(HitIndicatorController newTarget) { 
        currentTarget = newTarget.transform;
        directionalIndicator.DirectionalTarget = currentTarget;
    }
}
