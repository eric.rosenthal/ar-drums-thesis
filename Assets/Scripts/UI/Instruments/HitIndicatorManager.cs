using System.Collections.Generic;
using System.Linq;

public class HitIndicatorManager : Singleton<HitIndicatorManager> {
    HitIndicatorController currentActiveIndicator;
    List<HitIndicatorController> indicatorList;

    OffscreenIndicatorManager offscreenIndicatorManager;
    BeatManager beatManager;

    protected override void Awake() {
        indicatorList = gameObject.GetComponentsInChildren<HitIndicatorController>().ToList();
    }

    void Start() {
        offscreenIndicatorManager = OffscreenIndicatorManager.GetInstance();
        indicatorList.ForEach(it => it.Deactivate());

        BeatManager.BeatSwitchedEvent += DeactivateCurrentIndicator;
        ResetPublisher.ResetEvent += Reset;
    }

    // only indicate the next instrument, when a new count is scheduled
    public void Indicate(Instruments instrument, double nextHitScheduledTime, double now) {
        HitIndicatorController indicator = indicatorList.Find(it => it.instrument == instrument);

        indicator.Activate();
        indicator.Animate(nextHitScheduledTime, now);
        offscreenIndicatorManager.ChangeTarget(indicator);
        currentActiveIndicator = indicator;
    }

    public void DeactivateCurrentIndicator() {
        currentActiveIndicator?.Deactivate();
    }

    void Reset() {
        DeactivateCurrentIndicator();
        currentActiveIndicator = null;
    }
}
