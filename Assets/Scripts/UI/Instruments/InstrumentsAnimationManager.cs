using System.Collections.Generic;
using UnityEngine;

public class InstrumentsAnimationManager : MonoBehaviour {
    BeatManager beatManager;
    Dictionary<Instruments, Animator> instrumentAnimatorMapping;
    Dictionary<Instruments, ParticleSystem> instrumentEmitterMapping;

    void Awake() {
        BeatVisualizer.VisualizeInstrumentEvent += TriggerAnimationForCorrectInstrument;
    }

    void Start() {
        beatManager = BeatManager.GetInstance();

        instrumentAnimatorMapping = new Dictionary<Instruments, Animator> {
            { Instruments.HiHat, gameObject.transform.Find(Instruments.HiHat.ToString()).GetComponent<Animator>() },
            { Instruments.Snare, gameObject.transform.Find(Instruments.Snare.ToString()).GetComponent<Animator>() },
            { Instruments.HiTom, gameObject.transform.Find(Instruments.HiTom.ToString()).GetComponent<Animator>() },
            { Instruments.MidTom, gameObject.transform.Find(Instruments.MidTom.ToString()).GetComponent<Animator>() },
            { Instruments.FloorTom, gameObject.transform.Find(Instruments.FloorTom.ToString()).GetComponent<Animator>() },
            { Instruments.Kick, gameObject.transform.Find(Instruments.Kick.ToString()).GetComponent<Animator>() }
        };

        instrumentEmitterMapping = new Dictionary<Instruments, ParticleSystem> {
            { Instruments.HiHat, gameObject.transform.Find(Instruments.HiHat.ToString()).GetComponentInChildren<ParticleSystem>() },
            { Instruments.Snare, gameObject.transform.Find(Instruments.Snare.ToString()).GetComponentInChildren<ParticleSystem>() },
            { Instruments.HiTom, gameObject.transform.Find(Instruments.HiTom.ToString()).GetComponentInChildren<ParticleSystem>() },
            { Instruments.MidTom, gameObject.transform.Find(Instruments.MidTom.ToString()).GetComponentInChildren<ParticleSystem>() },
            { Instruments.FloorTom, gameObject.transform.Find(Instruments.FloorTom.ToString()).GetComponentInChildren<ParticleSystem>() },
            { Instruments.Kick, gameObject.transform.Find(Instruments.Kick.ToString()).GetComponentInChildren<ParticleSystem>() }
        };
    }

    // Musical count starts at one, so adjust to zero for beat array
    void TriggerAnimationForCorrectInstrument(int count) {
        Beat currentBeat = beatManager.activeBeat;
        Instruments instrument = currentBeat.sequence[count - 1].instrument;
        Animator animator = instrumentAnimatorMapping[instrument];
        ParticleSystem emitter = instrumentEmitterMapping[instrument];
        animator.SetTrigger("Play");
        emitter.Play();
    }
}
