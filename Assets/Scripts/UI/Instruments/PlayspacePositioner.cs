using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayspacePositioner : MonoBehaviour {
    Transform cam;
    List<InstrumentPositionController> instruments;

    void Start() {
        cam = Camera.main.transform;
        instruments = gameObject.GetComponentsInChildren<InstrumentPositionController>().ToList();
        Reset();

        ResetPublisher.ResetEvent += Reset;
    }

    public virtual void Reset() { 
        Debug.Log("PlayspacePositioner - Resetting playspace");
        instruments.ForEach(it => it.Reset());
        transform.position = cam.position + (cam.forward / 20) + new Vector3(0, 0.02f, 0);
        transform.LookAt(cam);
        transform.Rotate(Vector3.up - new Vector3(0,180,0));
    }
}
